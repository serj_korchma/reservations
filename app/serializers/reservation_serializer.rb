class ReservationSerializer < ActiveModel::Serializer
  attributes :id, :table_number, :start_time, :end_time

  #convert to timestamp for angular js
  def start_time
    object.start_time.to_time.to_i
  end

  #convert to timestamp for angular js
  def end_time
    object.end_time.to_time.to_i
  end

end